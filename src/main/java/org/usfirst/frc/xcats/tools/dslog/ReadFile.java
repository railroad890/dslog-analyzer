package org.usfirst.frc.xcats.tools.dslog;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadFile {

	private String mInputFile;
	private String mSeparator;

	public ReadFile(String inputFile, String separator) {
		mInputFile = inputFile;
		mSeparator = separator;
	}

	public ArrayList readFile() {

		String line;
		ArrayList<ArrayList<String>> output = new ArrayList<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(mInputFile))) {

			while ((line = reader.readLine()) != null) {

				ArrayList<String> values = new ArrayList<>();

				String[] lineValues = line.split(mSeparator);

				for (String value : lineValues) {
					values.add(value);
				}

				output.add(values);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return output;
	}
}
